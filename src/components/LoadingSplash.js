import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import React from 'react';
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    height: '300px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }
}));
export default function LoadingSplash() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <CircularProgress />
    </div>
  );
}
